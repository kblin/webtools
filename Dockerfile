# Bioinformatic webtools
# VERSION 0.1.0
FROM debian:jessie
MAINTAINER Kai Blin <kblin@biosustain.dtu.dk>

# grab all the dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        build-essential \
        gunicorn \
        ncbi-blast+ \
        python-biopython \
        python-dev \
        python-pip \
        && \
    apt-get clean -y && \
    apt-get autoremove -y

# Grab antiSMASH
ADD . /app/

WORKDIR /app
RUN dd if=/dev/urandom bs=32 count=1 | base64 > /app/secret.txt && sed -e "s/__SECRET__/`cat /app/secret.txt`/g" && rm /app/secret.txt
RUN pip install -r requirements.txt

ENTRYPOINT ["gunicorn"]
CMD ["-b", "0.0.0.0:8000", "webtools:app"]

VOLUME ["/data"]
EXPOSE 8000
