#!/usr/bin/env python

import re
from urllib import urlretrieve
from tempfile import mkstemp
from os import path
from glob import glob
from flask import Flask, render_template, request, send_from_directory, flash,\
                  jsonify, redirect
from helperlibs.bio.featurematch import find_features
from helperlibs.bio import seqio
import subprocess

app = Flask(__name__)
app.secret_key = "__SECRET__"
app.config['MAX_CONTENT_LENGTH'] = 50 * 1024 * 1024

ALLOWED_BLAST_EXTENSIONS = set(['fa', 'fna', 'faa', 'fasta'])
GENOME_FOLDER = "/data/genomes/complete"
BLASTDB_FOLDER = "/data/genomes/blastdb"
SEQ_FOLDER = "/data/genomes/fasta"


def allowed_blast_file(filename):
    """Check if the filename looks valid for BLAST"""
    return allowed_file(filename, ALLOWED_BLAST_EXTENSIONS)


def allowed_file(filename, extensions):
    """Check if the filename looks valid"""
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in extensions


@app.route('/')
def index():
    return render_template('index.html')


def _name_from_genome_file(filename):
    name, ext = path.splitext(filename)
    return name.replace('_', ' ')


def _get_organisms():
    available_files = glob(path.join(GENOME_FOLDER, '*.gbk'))
    available_files.extend(glob(path.join(GENOME_FOLDER, '*.embl')))
    available_files = map(path.basename, available_files)
    available_files.sort()
    return available_files


def _get_locus_tag_pattern(filename):
    full_path = path.join(GENOME_FOLDER, path.basename(filename))
    with open(full_path, 'r') as handle:
        seq_rec = seqio.read(handle)

    for feature in seq_rec.features:
        if 'locus_tag' in feature.qualifiers:
            return feature.qualifiers['locus_tag'][0]

    return None


def _name_from_blastdb(filename):
    name, ext = path.splitext(filename)
    return name.replace('_', ' ').capitalize()


def _get_blastdbs():
    available_files = glob(path.join(BLASTDB_FOLDER, '*.nin'))
    available_files = map(path.basename, available_files)
    available_files = map(lambda x: path.splitext(x)[0], available_files)
    available_files.sort()
    return available_files


def _get_seqfiles():
    available_files = glob(path.join(SEQ_FOLDER, '*.fa'))
    available_files = map(path.basename, available_files)
    available_files.sort()
    return available_files


def _find_given_feature(locus_tag, organism, output_format, uploaded):
    if organism != "uploaded":
        with open(path.join(GENOME_FOLDER, organism)) as handle:
            seqs = seqio.parse(handle)
            features = find_features(seqs, locus_tag)
    else:
            print "%r" % uploaded.name
            seqs = seqio.parse(uploaded)
            features = find_features(seqs, locus_tag)

    if output_format == "dna":
        feature = "\n".join(map(lambda x: x.dna_fasta(), features))
    elif output_format == "long_dna":
        feature = "\n".join(map(lambda x: x.long_dna_fasta(), features))
    elif output_format == "mrna":
        feature = "\n".join(map(lambda x: x.mrna_fasta(), features))
    elif output_format == "protein":
        feature = "\n".join(map(lambda x: x.protein_fasta(), features))
    elif output_format == "promotor":
        feature = "\n".join(map(lambda x: x.promotor_fasta(), features))
    else:
        feature =  "\n".join(map(str, features))
    return feature


@app.route('/get_feature', methods=['GET', 'POST'])
def get_feature():
    available_organisms = _get_organisms()
    try:
        if request.method == 'POST':
            locus_tag = request.form.get('locus_tag', '').strip()
            if locus_tag == '':
                flash('No locus tag specified')
                raise ValueError()

            output_format = request.form.get('format', 'text').strip()

            organism = request.form.get('organism', '').strip()
            if organism == '':
                flash('No organism specified')
                raise ValueError()

            if organism != "uploaded" and organism not in available_organisms:
                flash('Organism %s is not available' % organism)
                raise ValueError()

            uploaded_file = request.files['file']
            feature = _find_given_feature(locus_tag, organism, output_format, uploaded_file)
            if feature == "":
                feature = "Locus tag %s not found in %s" % (locus_tag, organism)

            return render_template('get_feature_results.html', tag=locus_tag,
                                   organism=organism, feature=feature)
    except ValueError:
        pass

    organism_options = map(lambda x: (_name_from_genome_file(x), x),
                           available_organisms)
    return render_template('get_feature.html', organisms=organism_options)


@app.route('/blast', methods=['GET', 'POST'])
def blast():
    available_dbs = _get_blastdbs()
    try:
        if request.method == 'POST':
            uploaded_file = request.files['file']
            if uploaded_file and allowed_blast_file(uploaded_file.filename):
                sequence = uploaded_file.read()
            elif request.form['paste'] is not None and request.form['paste'].strip():
                sequence = request.form['paste']
            else:
                flash("No valid input sequence found")
                raise ValueError()

            seqtype = request.form.get('seqtype', '').strip()
            if seqtype not in ('nucl', 'prot'):
                flash("Invalid algorithm selected")
                raise ValueError()

            blastdb = request.form.get('blastdb', '').strip()
            if blastdb not in available_dbs:
                flash("Database %s is not available" % blastdb)
                raise ValueError()

            command = 'blastp' if seqtype == 'prot' else 'blastn'

            command_line = [command, '-db', path.join(BLASTDB_FOLDER, blastdb),
                            '-html']


            p = subprocess.Popen(command_line, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            result, error = p.communicate(sequence)

            return result

    except ValueError:
        pass
    except OSError:
        flash("Error running BLAST")
        pass

    blastdb_options = map(lambda x: (_name_from_blastdb(x), x), available_dbs)
    return render_template('blast.html', blastdbs=blastdb_options)


@app.route('/to_blast', methods=['POST'])
def to_blast():
    available_dbs = _get_blastdbs()
    sequence = request.form.get('feature', 'Failed to read sequence').strip()

    blastdb_options = map(lambda x: (_name_from_blastdb(x), x), available_dbs)
    return render_template('blast.html', blastdbs=blastdb_options, sequence=sequence)


@app.route('/get_example_locus_tag/<organism>')
def get_example_locus_tag(organism):
    available_organisms = _get_organisms()
    if organism not in available_organisms:
        return jsonify(error="Failed to get locus_tag")

    locus_tag = _get_locus_tag_pattern(organism)
    return jsonify(locus_tag=locus_tag)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8000)
